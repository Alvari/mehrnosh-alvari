import { Component } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'finalProject';

  constructor(private loginService: LoginService, private route: Router) {


  }

  logout() {
    this.loginService.clearLocalStorage();
    this.route.navigateByUrl("/login");
  }

  showLogout() {
    return this.loginService.isLogin();
  }
}
