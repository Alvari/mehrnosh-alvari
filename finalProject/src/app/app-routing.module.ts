import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { ChatComponent } from './chat/chat.component';
import { EmpolyeeAddComponent } from './empolyee-add/empolyee-add.component';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', redirectTo: '/employee', pathMatch: 'full' },
  {
    component: EmployeeComponent,
    path: "employee",
    canActivate: [AuthGuardService]

  },
  {
    path: "employee/add",
    component: EmpolyeeAddComponent,
    canActivate: [AuthGuardService]
  },
  {
    component: ChatComponent,
    path: "chat",
    canActivate: [AuthGuardService]
  },
  {
    component: LoginComponent,
    path: "login",

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
