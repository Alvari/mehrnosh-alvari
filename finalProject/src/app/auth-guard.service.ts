import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';


@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private route: Router,
        private loginSrv: LoginService) {
    }

    canActivate(route: ActivatedRouteSnapshot,

        state: RouterStateSnapshot): boolean {

        //check some condition  
        if (!this.loginSrv.isLogin()) {
            alert('لطفاً لاگین کنید');
            this.route.navigate(['/login']);
            return false;
        }
        return true;
    }

}