import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { Employee } from '../model/employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empolyee-add',
  templateUrl: './empolyee-add.component.html',
  styleUrls: ['./empolyee-add.component.css']
})
export class EmpolyeeAddComponent implements OnInit {

  constructor(
    private empSrv: EmployeeService,
    private route: Router

  ) { }
  employeeAddForm: FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    age: new FormControl(''),
    salary: new FormControl(''),
  });
  ngOnInit(): void {

  }

  add() {
    var formdata = this.employeeAddForm.getRawValue();
    const data: Employee = {
      age: formdata.age,
      id: 0,
      name: formdata.firstName + " " + formdata.lastName,
      salary: formdata.salary
    }
    this.empSrv.saveEmployee(data).subscribe(res => {
      if (res.status === "success") {
        alert("کاربر ثبت شد");
        this.route.navigate(['/employee']);
      } else {
        alert("کاربر ثبت نشد");
      }
    });
  }

}
