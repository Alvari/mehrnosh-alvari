import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private route: Router) { }

  ngOnInit(): void {
  }

  loginForm: FormGroup = new FormGroup({
    userName: new FormControl('admin'),
    password: new FormControl('admin')
  })

  login() {
    var user = this.loginForm.getRawValue();
    if (user.userName == 'admin' && user.password == 'admin') {
      this.loginService.setUserLoggedIn(user);
      this.route.navigate(['/']);
    } else {
      alert('نام کاربری یا رمز عبور اشتباه است');
    }
  }

}
