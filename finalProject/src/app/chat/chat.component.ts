import { Component, OnInit } from '@angular/core';
import { message } from '../model/message';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  messageList: message[] = []
  chatFormB: FormGroup = new FormGroup({
    message: new FormControl('')
  });
  chatFormA: FormGroup = new FormGroup({
    message: new FormControl('')
  });

  constructor() { }

  ngOnInit(): void {
  }

  send(sender) {
    const form = sender == "1" ? this.chatFormA : this.chatFormB;
    this.messageList.push({
      sender: sender,
      text: form.get("message").value
    });
  }
}

