import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class LoginService {

    setUserLoggedIn(user: any) {
        localStorage.setItem('user', JSON.stringify(user));
    }

    getUserLoggedIn() {
        let result = null;
        if (localStorage.getItem('user')) {
            result = JSON.parse(localStorage.getItem('user'));
        } else {
            console.log('localStorage empty');
        }
        return result
    }

    isLogin() {
        let result = false;
        if (localStorage.getItem('user'))
            result = true;
        return result;
    }
    clearLocalStorage() {
        localStorage.clear();
    }

}