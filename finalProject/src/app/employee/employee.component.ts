import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../model/employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private restClient: EmployeeService,
    private route: Router
  ) {

  }


  employeeList = [];
  employee: Employee;

  ngOnInit() {
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.restClient.getEmployees().subscribe(res => {
      this.employeeList = res.data;
    });
  }

  displayedColumns: string[] = ['id', 'employee_name', 'employee_salary', 'employee_age'];

  delete(id: number) {
    this.restClient.deleteEmployee(id).subscribe(res => {
      if (res.status === "success") {
        alert("کاربر حذف شد");
        this.route.navigate(['/employee']);
      } else {
        alert("کاربر حذف نشد");
      }
    });
  }

  addNew() {
    this.route.navigate(['/employee/add']);
  }

}
